package com.techuniversity;


import com.techuniversity.lib.Saludos;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //Levanto instancia de saludos
        Saludos saludos = new Saludos();
        //le pido un saludo informarl
        String strSaludo = saludos.getSaludoInformal("Enric");
        //lo saco por consola
        System.out.println( strSaludo );

    }
}
